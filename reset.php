<?php 

$xml = simplexml_load_file('app/etc/local.xml');
$connection = $xml->xpath('global/resources/default_setup/connection');
$connection = $connection[0];

$table_prefix = $xml->xpath('global/resources/db');
$table_prefix = $table_prefix[0];
$table_prefix = $table_prefix->table_prefix;

$host= $connection->host; 
$user= $connection->username;
$pwd= $connection->password;
$db_name = $connection->dbname;

$db = mysqli_connect($host, $user, $pwd);
if (!$db) {
    die('Could not connect: ' . mysqli_error($db));
}

mysqli_select_db($db,$db_name);
$filename = 'var/log/IPSecurity.log';

if(file_exists($filename)){
    echo 'Log already exists, please delete it!';
}

if(isset($_POST['submit']) && !empty($_POST['reset_option']) && !file_exists($filename)){
        $reset_option = $_POST['reset_option'];
        $text = "*** IP Security Log *** \n\n";

        if($reset_option === 'frontend' || $reset_option === 'all'){
            $frontend_allow = mysqli_query($db, 'SELECT value FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityfront/allow'");
            $frontend_allow = mysqli_fetch_row ($frontend_allow);
            $frontend_block = mysqli_query($db, 'SELECT value FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityfront/block'");
            $frontend_block = mysqli_fetch_row ($frontend_block);

            $text .= "Frontend Allow this IP's: \n";
            $text .= $frontend_allow[0] . "\n\n";
            $text .= "Frontend Block this IP's: \n";
            $text .= $frontend_block[0] . "\n\n";

            mysqli_query($db, 'DELETE FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityfront/allow'");
            mysqli_query($db, 'DELETE FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityfront/block'");
        }
        if($reset_option === 'admin' || $reset_option === 'all'){
            $admin_allow = mysqli_query($db, 'SELECT value FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityadmin/allow'");
            $admin_allow = mysqli_fetch_row ($admin_allow);
            $admin_block = mysqli_query($db, 'SELECT value FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityadmin/block'");
            $admin_block = mysqli_fetch_row ($admin_block);

            $text .= "Admin Allow this IP's: \n";
            $text .= $admin_allow[0] . "\n\n";
            $text .= "Admin Block this IP's: \n";
            $text .= $admin_block[0] . "\n\n";

            mysqli_query($db, 'DELETE FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityadmin/block'");
            mysqli_query($db, 'DELETE FROM ' .$table_prefix."core_config_data where path='etipsecurity/ipsecurityadmin/allow'");
        }

        if(file_put_contents ($filename, $text)){
            echo '<h4>Log successfully saved!</h4>';
            echo '<h4>IP Security reset success!</h4>';
        }
        chmod($filename, 0666);
}
else{
    echo "IP Security Reset Failed!";
}
mysqli_close($db);
?>

<?php
    if(!file_exists($filename) && (!isset($_POST['submit']) ||  empty($_POST['reset_option']))):?>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
    <p>
        <input type="radio" name="reset_option" value="frontend"/>Delete Frontend IP's
    </p>
    <p>
        <input type="radio" name="reset_option" value="admin"/>Delete Admin IP's
    </p>
    <p>
        <input type="radio" name="reset_option" value="all"/>Delete Frontend and Admin IP's
    </p>
    <input type="submit" value="Submit" name="submit"/>
</form>
<?php endif; ?>
