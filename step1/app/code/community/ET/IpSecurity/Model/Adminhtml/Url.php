<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_IpSecurity
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_IpSecurity_Model_Adminhtml_Url
 */
class ET_IpSecurity_Model_Adminhtml_Url extends Mage_Adminhtml_Model_Url
{
    /**
     * add security token to url if( enabled security token) 
     * (need for uploading images in wysiwyg editor & product edit / images )
     * 
     * @inheritdoc
     * 
     * @return $this
     */
    public function addSessionParam()
    {
        parent::addSessionParam();
        /** @var ET_IpSecurity_Helper_Data $etHelper */
        $etHelper = Mage::helper('etipsecurity');
        
        if ($etHelper->isEnabledIpSecurityToken()) {
            $this->setQueryParam(
                $etHelper->getTokenName(),
                $etHelper->getTokenValue()
            );
        }
        return $this;
    }
}