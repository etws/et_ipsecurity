<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_IpSecurity
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

/**
 * Class ET_IpSecurity_Block_Adminhtml_Media_Uploader
 */
class ET_IpSecurity_Block_Adminhtml_Media_Uploader extends Mage_Adminhtml_Block_Media_Uploader
{
    /**
     * add token for flex uploader
     * 
     * @inheritdoc
     * 
     * @return string
     */
    public function getConfigJson()
    {
        /** @var ET_IpSecurity_Helper_Data $etHelper */
        $etHelper = Mage::helper('etipsecurity');

        if (!$etHelper->isEnabledIpSecurityToken()) {
            return parent::getConfigJson();
        } else {

            /** @var Varien_Object $config */
            $config = $this->getConfig();
            /** @var Mage_Adminhtml_Model_Url $urlModel */
            $urlModel = Mage::getModel('adminhtml/url');

            $urlModel->setQueryParam(
                $etHelper->getTokenName(),
                $etHelper->getTokenValue()
            );

            $config->setUrl(
                $urlModel->addSessionParam()->getUrl('*/catalog_product_gallery/upload/')
            );
            
            return Mage::helper('core')->jsonEncode($config->getData());
        }
    }
    
}